import pyscreenshot as ImageGrab
import smokemsg
import threading
import time


def test_alert():
    t = threading.Thread(target=smokemsg.alert)
    t.daemon = True
    t.start()

    time.sleep(1)

    # grab fullscreen
    im = ImageGrab.grab()

    # save image file
    im.save('alert.png')

    return True


def test_confirm():
    t = threading.Thread(target=smokemsg.confirm)
    t.daemon = True
    t.start()

    time.sleep(1)

    # grab fullscreen
    im = ImageGrab.grab()

    # save image file
    im.save('confirm.png')

    return True
