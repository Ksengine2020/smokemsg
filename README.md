[![Twitter](https://img.shields.io/twitter/url?color=blue&logo=twitter&style=for-the-badge&url=https%3A%2F%2Fgithub.com%2FQuantumGUI%2Fsmokemsg)](https://twitter.com/intent/tweet?text=Wow:&url=https%3A%2F%2Fgithub.com%2FQuantumGUI%2Fsmokemsg)
[![GitHub issues](https://img.shields.io/github/issues/QuantumGUI/smokemsg?logo=github&style=for-the-badge)](https://github.com/QuantumGUI/smokemsg/issues)
[![GitHub forks](https://img.shields.io/github/forks/QuantumGUI/smokemsg?logo=github&style=for-the-badge)](https://github.com/QuantumGUI/smokemsg/network)
[![GitHub stars](https://img.shields.io/github/stars/QuantumGUI/smokemsg?logo=github&style=for-the-badge)](https://github.com/QuantumGUI/smokemsg/stargazers)
[![GitHub license](https://img.shields.io/github/license/QuantumGUI/smokemsg?logo=github&style=for-the-badge)](https://github.com/QuantumGUI/smokemsg/blob/main/LICENSE)
![GitHub Workflow Status](https://img.shields.io/github/workflow/status/QuantumGUI/smokemsg/Python%20package?logo=github&style=for-the-badge)
![GitHub top language](https://img.shields.io/github/languages/top/QuantumGUI/smokemsg?logo=github&style=for-the-badge)
![PyPI - Downloads](https://img.shields.io/pypi/dd/SmokeMSG?logo=pypi&style=for-the-badge)

<br />
<p align="center">
    <a href="#SmokeMSG">
    <img src="https://github.com/QuantumGUI/smokemsg/raw/main/docs/assets/logo.png" alt="Logo" width="80" height="80">
    </a>
  <h3 id="SmokeMSG" align="center">SmokeMSG</h3>

  <p align="center">
    Best Python Library To Show Simple Dialogs.
    <br />
    <br />
    <a href="https://quantumgui.github.io/smokemsg/"><strong>Explore the docs Â» :book:</strong></a>
    <br />
    <br />
    <a href="https://quantumgui.github.io/smokemsg/demo">View Demo</a>
    Â·
    <a href="https://github.com/QuantumGUI/smokemsg/issues">Report Bug</a>
    Â·
    <a href="https://github.com/QuantumGUI/smokemsg/issues">Request Feature</a>
  </p>
</p>


<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about">About</a>
      <ul>
        <li><a href="#features">Features</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#installation">Installation</a></li>
        <li><a href="#example">Example</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>


## About

|![Linux](docs/assets/linux.png)|![Linux](docs/assets/macos.png)|![Windows](docs/assets/windows.png)|
|:-:|:-:|:-:|
|**Linux**|**macOS**|**Windows**|

SmokeMSG provides following functions to show dialogs.
- [x] :warning: alert
- [x] :question: confirm 
- [ ] :pencil: prompt

### Features

- **Pure** Python:snake:
- **No dependencies**(only depends on standard Python library)
- works on Python **3** and Python **2**
- **Cross platform** :heavy_plus_sign:
  - [x] <img src="https://microsoft.com/favicon.ico" width="16" height="16"><sup>Â®</sup> Windows
  - [x] <img src="https://linux.org/favicon.ico" width="16" height="16"><sup>Â®</sup> Linux(most ones anyway, including Raspbian)
  - [x] **macOS<sup>Â®</sup>** OS X
  - [ ] <img src="https://android.com/favicon.ico" width="16" height="16"><sup>Â®</sup> Android
  - [ ] **iOS<sup>Â®</sup>** iOS(Apple phones)
- **Easy packaging** - no fancy (and heavy) GUI libraries. Easy to use with cx_Freeze and PyInstaller.


## Getting Started

### Installation

```bash
pip install smokemsg
```
You may want to use `py -m pip`, `python -m pip` or `python3 -m pip` depending on your python installation and platform.

### Example:
    
```py
import smokemsg

if smokemsg.confirm('Are You Sure?', 'Question'):
    print('Sure...')
```


## License

Licensed under MIT license. See [LICENSE](/license) for details.

<a href="mailto:?Subject=Best+Python+Library+To+Show+Simple+Dialogs&amp;Body=SmokeMSG-Best+Python+Library+To+Show+Simple+Dialogs.+https%3A%2F%2Fgithub.com%2FQuantumGUI%2Fsmokemsg">
    <img src="https://simplesharebuttons.com/images/somacro/email.png" alt="Email"  width="64" height="64" />Â®
</a>
<a href="http://www.facebook.com/sharer.php?u=https%3A%2F%2Fgithub.com%2FQuantumGUI%2Fsmokemsg" target="_blank">
    <img src="https://simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook"  width="64" height="64" />Â®
</a>
<a href="https://twitter.com/intent/tweet?url=https%3A%2F%2Fgithub.com%2FQuantumGUI%2Fsmokemsg&text=SmokeMSG-Best+Python+Library+To+Show+Simple+Dialogs." target="_blank">
    <img src="https://simplesharebuttons.com/images/somacro/twitter.png" alt="Twitter" width="64" height="64" />Â®
</a>
<a href="http://reddit.com/submit?url=https%3A%2F%2Fgithub.com%2FQuantumGUI%2Fsmokemsg&amp;title=Best+Python+Library+To+Show+Simple+Dialogs." target="_blank">
    <img src="https://reddit.com/favicon.ico" alt="Reddit" width="64" height="64" />Â®
</a>
<a href="https://wa.me/?text=https%3A%2F%2Fgithub.com%2FQuantumGUI%2Fsmokemsg%0ASmokeMSG+-+Best+Python+Library+To+Show+Simple+Dialogs." target="_blank">
    <img src="https://whatsapp.com/favicon.ico" alt="WhatsApp" width="70" height="70" />Â®
</a>
<a href="http://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fgithub.com%2FQuantumGUI%2Fsmokemsg&text=SmokeMSG%20-%20Best%20Python%20Library%20To%20Show%20Simple%20Dialogs." target="_blank">
    <img src="https://content.linkedin.com/content/dam/me/business/en-us/amp/brand-site/v2/bg/LI-Bug.svg.original.svg" alt="Linkedin" width="64" height="64" />Â®
</a>